package main

import (
	"fmt"
	"net/http"
)

func main() {

	// create a new `ServeMux`
	mux := http.NewServeMux()

	// handle `/` route
	mux.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		fmt.Fprint(res, "Hello World!")
	})

	// handle `/hello/golang` route
	mux.HandleFunc("/hello/golang", func(res http.ResponseWriter, req *http.Request) {
		a := make([]string, 1)
		a[2] = "foo"
		fmt.Fprint(res, "Hello Golang! %v", a)
	})

	// listen and serve using `ServeMux`
	http.ListenAndServe(":9000", mux)

}
