FROM docker.io/nginx:1.20
RUN apt-get update && apt-get install -y libgnutls30
COPY http.js /etc/nginx
COPY nginx.conf /etc/nginx
COPY default.conf /etc/nginx/conf.d/
COPY welcome-green.html /usr/share/nginx/html/index.html
