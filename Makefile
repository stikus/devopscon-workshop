###
#  general configuration of the makefile itself
###
SHELL := /bin/bash
SHELLOPTS := -o nopipefail
.DEFAULT_GOAL := help

.PHONY: test

PY ?= python3

define PRINT_HELP_PYSCRIPT
# BSD 2-Clause License
#
# Copyright (c) 2021, Oz N Tiram <oz.tiram@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import re, sys
from collections import defaultdict

targets = {}
variables = []
local_variables = defaultdict(list)

print("Targets:\n")
for line in sys.stdin:
    match = re.search(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        targets.update({target: help})
    local_var = re.search(r'^(?P<target>[\w_\-/]+):.(?P<var>[A-Z_]+).*#\? (?P<help>.*)$$', line)
    if local_var:
        gd = local_var.groupdict()
        local_variables[gd['target']].append({'name': gd['var'], 'help': gd['help']})
        continue
    vars = re.search(r'([A-Z_]+).*#\? (.*)$$', line)
    if vars:
        target, help = vars.groups()
        variables.append("%-20s %s" % (target, help))

for target, help in targets.items():
    print("%-20s %s\n" % (target, help))
    if target in local_variables:
        [print("  %s - %s " % (i['name'], i['help'])) for i in local_variables[target]]
    print("")
if variables:
    print("Global Variables you can override:\n")
    print('\n'.join(variables))

endef

export PRINT_HELP_PYSCRIPT
VERSION ?=$(shell git describe --always)#? version
REGISTRY ?=docker.io#? container registry to push
ORG ?=oz123#? organization to push to
IMG ?=$(shell basename $(CURDIR))#? image name
OPTS ?= #? add extra OPTS to misc commands

help:
	@$(PY) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

test-all:
	 $(MAKE) test-color
	 $(MAKE) test-color COLOR=red || echo "OK! The color is not red"

test-color: HOST ?= localhost#? what host is running NGinx
test-color: PORT := 8080#? the port which accepts connections
test-color: COLOR ?= green#? which color should be the backround
test-color: ## test that the HOST shows background COLOR
	curl -s -k -L $(HOST):$(PORT) | grep -qc "background: $(COLOR)"

integration-test:: docker-clean docker-build docker-run test-all docker-clean

docker-build::  ## build a docker image
	docker build $(OPTS) -t $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION) -f Dockerfile .

docker-push::  ## push docker image to $(REGISTRY)
	docker push $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION)

docker-run:: OPTS := --rm -p 8080:80 -d --name $(subst -,_,$(IMG))_$(VERSION)
docker-run::
	docker run $(OPTS) $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION)
	sleep 2; # add this so curl after run does not fail, nginx needs a while to start

docker-test: ## run the test in docker
	docker exec $(OPTS) $(subst -,_,$(IMG))_$(VERSION) curl -s -k -L localhost | grep -qc "background: green"
	docker exec $(OPTS) $(subst -,_,$(IMG))_$(VERSION) curl -s -k -L localhost | grep -qc "background: red" || echo "Red color not found"

docker-clean::
	if docker ps | grep $(subst -,_,$(IMG))_$(VERSION); then docker stop $(subst -,_,$(IMG))_$(VERSION); fi
	docker image rm $(ORG)/$(IMG):v$(VERSION)

docker-lint:: ## runs Dockerfile analysis
	 docker run --rm -i hadolint/hadolint < Dockerfile

go-vet:
	cd go-server ; go vet .

pylint:
	pylint py-modules

docker-cve-scan:: SEVERITY := CRITICAL#? which severity level to consider
docker-cve-scan:: ## scan docker images for known CVEs
	trivy image $(OPTS) --exit-code 1 --severity $(SEVERITY) $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION)

k8s-patch:: ## update kustomization and patch with the correct versions
	sed -i "s/LATEST/v$(VERSION)/g" k8s/kustomization.yml
	sed -i "s/SSVERSIONSS/v$(VERSION)/g" k8s/patch.yml

k8s-kustomize:: # show what will be deployed
	kubectl $(OPTS) kustomize k8s/

k8s-deploy: ## deploy latest image to k8s
	kubectl $(OPTS) apply -k k8s/
# vim: tabstop=4 shiftwidth=4

